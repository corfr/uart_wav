/*
 * main.c
 *
 *  Created on: 27 nov. 2012
 *      Author: bro
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

#include <termios.h>
#include <fcntl.h>
#include <unistd.h>

typedef struct WavHeaderTag {
	struct {
		uint8_t		FileTypeBlocID[4];  // should always contain "RIFF"
		uint32_t 	FileSize;    		// total file length minus 8
		uint8_t		FileFormatID[4];    // should be "WAVE"
	} format;

	struct {
		uint8_t		FileFormatID[4];    // should be "fmt "
		uint32_t 	BlocSize;         	// 16 for PCM format
		uint16_t 	AudioFormat;        // 1 for PCM format
		uint16_t	nChannels;       	// channels
		uint32_t	nSamplesPerSec;     // sampling frequency
		uint32_t	nBytePerSec;
		uint16_t	nBytePerBloc;
		uint16_t	wBitsPerSample;
	} audio;

	struct {
		uint8_t		DataBlocID[4];        // should always contain "data"
		uint32_t	DataSize;
	} data;

}__attribute__((packed)) WavHeader;

void WavHeader_Init(WavHeader * header) {

	/* Format */
	memcpy(&(header->format.FileTypeBlocID), "RIFF", 4);
	header->format.FileSize = 0;
	memcpy(&(header->format.FileFormatID), "WAVE", 4);

	/* Audio */
	memcpy(&(header->audio.FileFormatID), "fmt ", 4);
	header->audio.BlocSize = 16;
	header->audio.AudioFormat = 1;
	header->audio.nChannels = 1;
	header->audio.nSamplesPerSec = 11025;
	header->audio.nBytePerSec = 2*11025;
	header->audio.nBytePerBloc = 2;
	header->audio.wBitsPerSample = 16;

	/* Data */
	memcpy(&(header->data.DataBlocID), "data", 4);
	header->data.DataSize = 0;
}


typedef struct Wav_FileDescriptorTag {
	WavHeader 	header;
	FILE 		* file;
	uint32_t	sample_count;
} Wav_FileDescriptor;

int Wav_Open(Wav_FileDescriptor * fd, char * path) {
	WavHeader_Init(&(fd->header));

	fd->sample_count = 0;

	fd->file = fopen(path, "wb");
	if(fd->file == NULL)
		return (-1);

	fwrite( &(fd->header), sizeof(fd->header), 1, fd->file);

	return 0;
}

void Wav_PushSample(Wav_FileDescriptor * fd, uint16_t sample) {
//	static uint8_t formated_sample[2] = {0};
//
//	formated_sample[0] = sample;

	fwrite(&sample, 2, 1, fd->file);
	fd->sample_count++;
}

void Wav_Close(Wav_FileDescriptor * fd) {

	/* Update Header */
	int data_size = fd->sample_count * 2;
	fd->header.format.FileSize = data_size + (44-8);
	fd->header.data.DataSize = data_size;

	/* Re-write header */
	fseek(fd->file, 0, SEEK_SET);
	fwrite( &(fd->header), sizeof(fd->header), 1, fd->file);

	fclose(fd->file);
}

int Uart_Open(char * port) {
	int fd;
	struct termios term;

	/* Ouverture du device */
	if ((fd = open(port, O_RDWR)) < 0) {
		perror(port);
		return -1;
	}

	/* Lecture des paramètres */
	tcgetattr(fd, &term);

	cfsetospeed(&term, B115200);
	cfsetispeed(&term, B115200);

    cfmakeraw(&term);

	/* On applique le nouveau paramètrage  */
	tcsetattr(fd, TCSANOW, &term);

	tcflush(fd, TCIOFLUSH);

	return fd;
}

struct termios Stdin_OldTerminalSettings;

#define STDIN 0  // file descriptor for standard input

void Stdin_Init()
{
	struct termios Stdin_NewTerminalSettings;

	// Get the current terminal settings
	if (tcgetattr(0, &Stdin_OldTerminalSettings) < 0)
		perror("tcgetattr()");

	memcpy(&Stdin_NewTerminalSettings, &Stdin_OldTerminalSettings, sizeof(struct termios));

	// disable canonical mode processing in the line discipline driver
	Stdin_NewTerminalSettings.c_lflag &= ~ICANON;

	// apply our new settings
	if (tcsetattr(0, TCSANOW, &Stdin_NewTerminalSettings) < 0)
		perror("tcsetattr ICANON");
}

void Stdin_Close()
{
	// Restore old settings
	if (tcsetattr(0, TCSANOW, &Stdin_OldTerminalSettings) < 0)
		perror("tcsetattr ICANON");
}

int Stdin_IsKeyPressed(void)
{
     struct timeval tv;
     fd_set fds;
     tv.tv_sec = 0;
     tv.tv_usec = 0;

     FD_ZERO(&fds);
     FD_SET(STDIN, &fds);

     select(STDIN+1, &fds, NULL, NULL, &tv);
     return FD_ISSET(STDIN, &fds);
}

int main(int argc, char * argv[]) {
	Wav_FileDescriptor fd;
	int uart_fd;
	uint8_t buffer[2];
	int size_rd;
	int16_t sample;

	/* Check */
	if( sizeof(WavHeader) != 44)  {
		printf("Size of header: %u", sizeof(WavHeader));
		return -1;
	}

	if(Wav_Open(&fd, "test.wav") == -1) {
		printf("Unable to write file\n");
		return -1;
	}

	Stdin_Init();

	uart_fd = Uart_Open( (argc > 1) ? argv[1] : "/dev/ttyUSB0" );
	if(uart_fd == -1) {
		printf("Unable to open serial port");
		Wav_Close(&fd);
		return -1;
	}

	while(!Stdin_IsKeyPressed()) {

		size_rd = read(uart_fd, buffer, 2);

		if(size_rd < 2) {
			size_rd += read(uart_fd, buffer+1, 1);
		}

		sample = (int16_t)(buffer[0] | (buffer[1] << 8));
		printf("[%6x] => {%5d} | {%5u}\n", sample, sample, sample);
		fflush(stdout);
		Wav_PushSample(&fd, (int16_t)sample);
	}

	// Test
//	srand(time(NULL));
//	for(i = 0; i < 10000; i++) {
//		Wav_PushSample(&fd, (uint8_t)(rand()) );
//	}

	Stdin_Close();

	Wav_Close(&fd);

	return 0;
}
